//
// Properties des abas Freetext-Editors
//
var editorOptions = {
  // correspond to A4 format
  width: '529pt',
  // minwidth in px
  minwidth: '350',
  // fontsize in pt
  fontsize: '12',
  fontfamily: 'DejaVu LGC Sans',
  fontcolor: '#000000',
  fonts: 'DejaVu LGC Sans,DejaVu LGC Sans-Bold,DejaVu LGC Sans-BoldOblique,DejaVu LGC Sans Condensed,DejaVu LGC Sans-ExtraLight,DejaVu LGC Sans Mono,DejaVu LGC Sans Mono-Bold,DejaVu LGC Sans Mono-BoldOblique,DejaVu LGC Sans Mono-Oblique,DejaVu LGC Sans-Oblique,DejaVu LGC Serif,DejaVu LGC Serif-Bold,DejaVu LGC Serif-BoldItalic,DejaVu LGC Serif Condensed,DejaVu LGC Serif Condensed-Bold,DejaVu LGC Serif Condensed-BoldItalic,DejaVu LGC Serif Condensed-Italic,DejaVu LGC Serif-Italic,DejaVu Sans,DejaVu Sans-Bold,DejaVu Sans-BoldOblique,DejaVu Sans Condensed,DejaVu Sans Condensed-Bold,DejaVu Sans Condensed-BoldOblique,DejaVu Sans Condensed-Oblique,DejaVu Sans-ExtraLight,DejaVu Sans Mono,DejaVu Sans Mono-Bold,DejaVu Sans Mono-BoldOblique,DejaVu Sans Mono-Oblique,DejaVu Sans-Oblique,DejaVu Serif,DejaVu Serif-Bold,DejaVu Serif-BoldItalic,DejaVu Serif Condensed,DejaVu Serif Condensed-Bold,DejaVu Serif Condensed-BoldItalic,DejaVu Serif Condensed-Italic,DejaVu Serif-Italic'
}